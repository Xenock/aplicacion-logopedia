class AddUserToPaciente < ActiveRecord::Migration[5.0]
  def change
    add_reference :pacientes, :user, foreign_key: true
  end
end
