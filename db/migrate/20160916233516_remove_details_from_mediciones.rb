class RemoveDetailsFromMediciones < ActiveRecord::Migration
  def change
    remove_column :mediciones, :oreja_comisura_izquierda, :float
    remove_column :mediciones, :oreja_comisura_derecha, :float
    remove_column :mediciones, :cupido_derecho, :float
    remove_column :mediciones, :cupido_izquierdo, :float
  end
end
