class AddPropertiesToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :nombre, :string
    add_column :users, :apellidos, :string
    add_column :users, :numero_colegiado, :integer
    add_column :users, :provincia, :string
    add_column :users, :localidad, :string
    add_column :users, :direccion, :string
    add_column :users, :telefono, :integer
  end
end
