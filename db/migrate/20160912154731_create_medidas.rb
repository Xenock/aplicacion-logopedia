class CreateMedidas < ActiveRecord::Migration
  def change
    create_table :medidas do |t|
      t.string :sexo
      t.integer :edad
      t.float :inferior
      t.float :superior
      t.string :zona

      t.timestamps null: false
    end
  end
end
