class CreateMediciones < ActiveRecord::Migration
  def change
    create_table :mediciones do |t|
      t.float :ojo_comisura_derecho
      t.float :ojo_comisura_izquierdo
      t.float :oreja_comisura_derecha
      t.float :oreja_comisura_izquierda
      t.float :cupido_izquierdo
      t.float :cupido_derecho
      t.float :leporino
      t.float :distancia_intercisiva_maxima
      t.float :trichion_menton
      t.float :tercio_superior
      t.float :tercio_medio
      t.float :tercio_inferior
      t.float :tercio_inferior_superior
      t.float :tercio_inferior_medio
      t.float :tercio_inferior_inferior
      t.references :paciente, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
