class CreatePacientes < ActiveRecord::Migration
  def change
    create_table :pacientes do |t|
      t.string :nombre
      t.string :apellidos
      t.string :telefono
      t.date :fecha_nacimiento
      t.string :motivo_consulta
      t.string :anamnesis
      t.string :sexo
      t.date :fecha_inicio_terapia

      t.timestamps null: false
    end
  end
end
