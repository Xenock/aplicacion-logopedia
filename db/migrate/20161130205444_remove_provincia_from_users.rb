class RemoveProvinciaFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :provincia, :string
  end
end
