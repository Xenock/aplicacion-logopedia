class AddDetailsToMediciones < ActiveRecord::Migration
  def change
    add_column :mediciones, :altura_labio_superior, :float
    add_column :mediciones, :altura_labio_inferior, :float
  end
end
