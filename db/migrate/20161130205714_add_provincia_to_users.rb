class AddProvinciaToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :provincia, foreign_key: true
  end
end
