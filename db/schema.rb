# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161130205714) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "mediciones", force: :cascade do |t|
    t.float    "ojo_comisura_derecho"
    t.float    "ojo_comisura_izquierdo"
    t.float    "leporino"
    t.float    "distancia_intercisiva_maxima"
    t.float    "trichion_menton"
    t.float    "tercio_superior"
    t.float    "tercio_medio"
    t.float    "tercio_inferior"
    t.float    "tercio_inferior_superior"
    t.float    "tercio_inferior_medio"
    t.float    "tercio_inferior_inferior"
    t.integer  "paciente_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.float    "altura_labio_superior"
    t.float    "altura_labio_inferior"
    t.index ["paciente_id"], name: "index_mediciones_on_paciente_id", using: :btree
  end

  create_table "medidas", force: :cascade do |t|
    t.string   "sexo"
    t.integer  "edad"
    t.float    "inferior"
    t.float    "superior"
    t.string   "zona"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pacientes", force: :cascade do |t|
    t.string   "nombre"
    t.string   "apellidos"
    t.string   "telefono"
    t.date     "fecha_nacimiento"
    t.string   "motivo_consulta"
    t.string   "anamnesis"
    t.string   "sexo"
    t.date     "fecha_inicio_terapia"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_pacientes_on_user_id", using: :btree
  end

  create_table "provincia", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "admin"
    t.string   "nombre"
    t.string   "apellidos"
    t.integer  "numero_colegiado"
    t.string   "localidad"
    t.string   "direccion"
    t.integer  "telefono"
    t.integer  "provincia_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["provincia_id"], name: "index_users_on_provincia_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "mediciones", "pacientes"
  add_foreign_key "pacientes", "users"
  add_foreign_key "users", "provincia", column: "provincia_id"
end
