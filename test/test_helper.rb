ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  def usuario_medicione
    @medicion = {
      ojo_comisura_derecho: @medicione.ojo_comisura_derecho,
      ojo_comisura_izquierdo: @medicione.ojo_comisura_izquierdo,
      oreja_comisura_derecha: @medicione.oreja_comisura_derecha,
      oreja_comisura_izquierda: @medicione.oreja_comisura_izquierda,
      cupido_izquierdo: @medicione.cupido_izquierdo,
      cupido_derecho: @medicione.cupido_derecho,
      gonio_menton_derecho: @medicione.gonio_menton_derecho,
      gonio_menton_izquierdo: @medicione.gonio_menton_izquierdo,
      leporino: @medicione.leporino,
      trichion_menton: @medicione.trichion_menton,
      tercio_superior: @medicione.tercio_superior,
      tercio_medio: @medicione.tercio_medio,
      tercio_inferior: @medicione.tercio_inferior,
      tercio_inferior_superior: @medicione.tercio_inferior_superior,
      tercio_inferior_medio: @medicione.tercio_inferior_medio,
      tercio_inferior_inferior: @medicione.tercio_inferior_inferior,
      paciente_id: @medicione.paciente_id }
  end
  # Add more helper methods to be used by all tests here...
end
