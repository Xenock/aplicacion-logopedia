require 'test_helper'

class PacientesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    sign_in users(:user)
    @paciente = pacientes(:manolo)
  end

  test "should get index" do
    get :index, params:{}
    assert_response :success
  end

  test "should get new" do
    get :new, params:{}
    assert_response :success
  end

  test "should create paciente" do
    assert_difference('Paciente.count') do
      post :create, params: {paciente: { fecha_inicio_terapia: @paciente.fecha_inicio_terapia, fecha_nacimiento: @paciente.fecha_nacimiento, motivo_consulta: @paciente.motivo_consulta, nombre: @paciente.nombre, sexo: @paciente.sexo }}
    end

    assert_redirected_to new_paciente_medicione_path(paciente_id: Paciente.last.id)
  end

  test "should show paciente" do
    get :show, params: { id: @paciente }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params:{ id: @paciente }
    assert_response :success
  end

  test "should update paciente" do
    patch :update, params: {id: @paciente, paciente: { fecha_inicio_terapia: @paciente.fecha_inicio_terapia, fecha_nacimiento: @paciente.fecha_nacimiento, motivo_consulta: @paciente.motivo_consulta, nombre: @paciente.nombre, sexo: @paciente.sexo }}
    assert_redirected_to paciente_path
  end

  test "should destroy paciente" do
    assert_difference('Paciente.count', -1) do
      delete :destroy, params:{ id: @paciente }
    end

    assert_redirected_to pacientes_path
  end
end
