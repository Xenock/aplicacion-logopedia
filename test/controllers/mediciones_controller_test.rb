require 'test_helper'

class MedicionesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  setup do
    sign_in users(:user)
    @medicione = mediciones(:uno)
  end

  test "should get index" do
    get :index, params: { paciente_id: @medicione.paciente }
    assert_response :success
    #assert_not_nil assigns(:mediciones)
  end

  test "should get new" do
    get :new, params: { paciente_id: @medicione.paciente }
    assert_response :success
  end

  # test "should create medicione" do
  #   assert_difference('Medicione.count') do
  #     post :create, params: {
  #            medicione: {
  #              ojo_comisura_derecho: @medicione.ojo_comisura_derecho,
  #              ojo_comisura_izquierdo: @medicione.ojo_comisura_izquierdo,
  #              altura_labio_superior: @medicione.altura_labio_superior,
  #              altura_labio_inferior: @medicione.altura_labio_inferior,
  #              leporino: @medicione.leporino,
  #              trichion_menton: @medicione.trichion_menton,
  #              tercio_superior: @medicione.tercio_superior,
  #              tercio_medio: @medicione.tercio_medio,
  #              tercio_inferior: @medicione.tercio_inferior,
  #              tercio_inferior_superior: @medicione.tercio_inferior_superior,
  #              tercio_inferior_medio: @medicione.tercio_inferior_medio,
  #              tercio_inferior_inferior: @medicione.tercio_inferior_inferior,
  #              paciente_id: @medicione.paciente_id }}
  #   end

  #   assert_redirected_to estatico_resultados_url(@medicione)
  # end

  test "should show medicione" do
    get :show, params: { id: @medicione, paciente_id: @medicione.paciente }
    assert_response :success
  end

  # test "should get edit" do
  #   get medicione_url, params: [ :id => @medicione, :paciente_id => @medicione.paciente ]
  #   assert_response :success
  # end

  # test "should update medicione" do
  #   patch :update, id: @medicione, medicione: { cupido_derecho: @medicione.cupido_derecho, cupido_izquierdo: @medicione.cupido_izquierdo, leporino: @medicione.leporino, ojo_derecho: @medicione.ojo_derecho, ojo_izquierdo: @medicione.ojo_izquierdo, oreja_derecha: @medicione.oreja_derecha, oreja_izquierda: @medicione.oreja_izquierda, paciente_id: @medicione.paciente_id }
  #   assert_redirected_to paciente_medicione_path(assigns(:medicione))
  # end

  # test "should destroy medicione" do
  #   assert_difference('Medicione.count', -1) do
  #     delete :destroy, id: @medicione
  #   end

  #   assert_redirected_to mediciones_path
  # end
end
