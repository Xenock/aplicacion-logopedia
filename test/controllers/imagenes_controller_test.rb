require 'test_helper'

class ImagenesControllerTest < ActionController::TestCase
  test "should get frontal" do
    get :frontal
    assert_response :success
  end

  test "should get perfil" do
    get :perfil
    assert_response :success
  end

end
