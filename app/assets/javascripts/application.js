//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require modernizr.inputForm.js
//= require underscore.js

//= require jquery-ui.js
//= require jquery-ui-rotatable.js
//= require jquery.ui.touch-punch.js
//= require jquery.cropit.js
//= require chartist.js
//= require chartist-plugin-tooltip.js

//= require sections/pacientes
//= require sections/imagenes
//= require sections/mediciones
//= require sections/resultados
//= require sections/graficas
//= require sections/show-paciente

$('document').ready(function() {
  var sections = [
      {
          selector : '#datos-paciente',
          class    : pacientes_class.new
      },
      {
          selector : '#imagen-frontal',
          class    : imagenes_class.frontal
      },
      {
          selector: '#datos-mediciones',
          class: mediciones_class.new
      },
      {
          selector: '#resultados',
          class: resultados_class.new
      },
      {
          selector: '#grafica',
          class: graficas_class.new
      },
      {
          selector: '#mostrar-paciente',
          class: show_paciente_class.new
      }
  ]

  _.each(sections, function(section) {
    $(section.selector).length && section.class($(section.selector));
  })
})
