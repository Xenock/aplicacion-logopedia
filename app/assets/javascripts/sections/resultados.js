var resultados_class = {
	  new : function(view) {
		    // Ver graficas
		    view.find('.ruta').on('click', function(e) {
            window.location.href = this.getAttribute('data-url')
		    })

		    // Go back
		    view.find('.go-back').on('click', function(e) {
			      window.history.back()
		    })
	  }
}
