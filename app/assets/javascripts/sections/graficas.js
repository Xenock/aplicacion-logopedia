var graficas_class = {
	  new : function(view) {
		    // Ver graficas
		    // view.find('.ruta').on('click', function(e) {
        //     window.location.href = this.getAttribute('data-url')
		    // })

		    // Go back
		    view.find('.go-back').on('click', function(e) {
			      window.history.back()
		    })

        new Chartist.Line('.ct-chart', {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
            series: [
                [12, 9, 7, 8, 5],
                [2, 1, 3.5, 7, 3],
                [1, 3, 4, 5, 6]
            ]
        }, {
            fullWidth: true,
            chartPadding: {
                right: 40
            }
        });
	  }
}
