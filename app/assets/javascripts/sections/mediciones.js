// Incluimos aquí el script para la sección de nueva medicion

var mediciones_class = {
	  new : function(view) {
		    // Nueva medicion
		    view.find('.new-medicion').on('click', function(e) {
			      $('#new_medicione').submit()
		    })

		    // Go back
		    view.find('.go-back').on('click', function(e) {
			      window.history.back()
		    })
	  }
}
