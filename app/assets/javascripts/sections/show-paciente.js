var show_paciente_class = {
    datos     : undefined,
    etiquetas : undefined,

    new : function(view) {
        $.getJSON('', function(data) {

            var results = []
            _.each(_.keys(data.mediciones[0]), function(result) { results[result] = new Array() })

            _.each(data.mediciones, function(mediciones, key) {
                _.each(mediciones, function(medicion, key) {
                    if(key !== 'created_at') {
                        if(_.last(data.mediciones)[key]) {
                            results[key].push({
                                meta: key,
                                value: medicion
                            })
                        }
                    }
                })
            })

            var final_results = new Array();
            _.each(_.pairs(results), function(re) {
                if(_.find(re[1], function(value) {
                    return value.value !== null
                })) {
                    final_results.push(re[1])
                }
            })

            new Chartist.Line('.ct-chart', {
                labels: _.map(data.mediciones, function(dato) {
                    return dato.created_at
                }),
                series: final_results,
                fullWidth: true,
                chartPadding: {
                    right: 40
                },
                plugins: [
                    Chartist.plugins.tooltip({
                        appendToBody: true
                    })
                ]
            })
        })
    }
}
