// Incluimos aquí el script para la sección de nuevo paciente

var imagenes_class = self = {
	imgContainer: undefined,
	rotatePreview: undefined,

	croped_image: undefined,
	rotate_degrees: undefined,
	top_tercio_percent: undefined,
	bottom_tercio_percent: undefined,

	top_tercio_drag: undefined,
	bottom_tercio_drag: undefined,

	frontal: function(view) {
		self.imgContainer = $('#image_treatment_area')

		var continue_btn = view.find('button.continue')
		var back_btn     = view.find('button.back')

		self.imgContainer.cropit({
			maxZoom         : 3,
			imageBackground : false,
			freeMove        : true,
			onImageLoaded   : function() {
				if(!self.isStepDone(1)) {
					self.setNextStep()
					self.appendCircle()
					continue_btn.prop('disabled', false)
				}

				self.rotatePreview = self.imgContainer.find('.image-preview-rotate')
			}
		});

		self.imgContainer.find('.select-image-btn').click(function() {
			self.imgContainer.find('.cropit-image-input').click()
		})

		continue_btn.on('click', function() {
			if(!self.isStepDone(2)) {
				self.croped_image = self.imgContainer.cropit('export', {
					type         : 'image/jpeg',
					quality      : .9,
					originalSize : true
				})

				self.setNextStep()
				back_btn.prop('disabled', false)
				self.imgContainer.cropit('disable')

				self.rotatePreview.rotatable({
					stop: function(e) {
						self.rotate_degrees = self.getRotationDegrees(self.rotatePreview.css('transform'))
					}
				})
			} else {
				self.setNextStep()

				back_btn.prop('disabled', true)
				self.rotatePreview.rotatable('destroy')

				self.top_tercio_drag.draggable('option', 'disabled', false);
				self.bottom_tercio_drag.draggable('option', 'disabled', false);
			}
		})

		back_btn.on('click', function() {
			self.imgContainer.cropit('reenable')
			self.rotatePreview.rotatable('destroy')
		})
	},

	appendCircle: function() {
		var backgroundContainer = self.imgContainer.find('.cropit-preview-image-container')

		if(!backgroundContainer.find('.circle').length) {
			backgroundContainer.append('<div class="circle"></div>')
				.find('.circle').append('<span class="vertical-line"></span>')

			backgroundContainer.find('.circle').append('<span class="bottom-line"><svg xmlns="http://www.w3.org/2000/svg" width="200" height="30" version="1.1"><path d="M10 0 Q 95 50 180 0" stroke="#FFF" stroke-width="4px" fill="transparent"></path></svg></span>')
			backgroundContainer.find('.circle').append('<span class="top-line"><svg xmlns="http://www.w3.org/2000/svg" width="200" height="60" version="1.1"><path d="M10 50 Q 95 0 180 50" stroke="#FFF" stroke-width="4px" fill="transparent"/></svg></span>')
		
			self.bottom_tercio_drag = backgroundContainer.find('.bottom-line')
			self.top_tercio_drag = backgroundContainer.find('.top-line')

			// Put the lines at 1/3 of positions
			var circleH = backgroundContainer.find('.circle').height()
			var tercios = circleH / 3

			var topTercioCorrection = tercios - 30
			self.top_tercio_drag.css('top', topTercioCorrection)

			var bottomTercioCorrection = tercios - 5
			self.bottom_tercio_drag.css('bottom', bottomTercioCorrection)
		}

		var dragOptions = {
			containment: backgroundContainer.find('.vertical-line'),
			axis: 'Y',
			disabled: true,
			drag: self.saveTercioDragPosition
		}

		self.top_tercio_drag.draggable(dragOptions)
		self.bottom_tercio_drag.draggable(dragOptions)
	},

	getRotationDegrees: function(matrix) {
		if(matrix !== 'none') {
			var values = matrix.split('(')[1].split(')')[0].split(',')
			var angle = Math.round(Math.atan2(values[0], values[0]) * (180/Math.PI))
		} else {
			var angle = 0
		}

		return (angle < 0) ? angle + 360 : angle;
	},

	saveTercioDragPosition: function() {
		var circleH = self.imgContainer.find('.cropit-preview-image-container').find('.circle').height()
		
		self.top_tercio_percent = (self.top_tercio_drag.css('top').split('px')[0] * 100) / circleH
		self.bottom_tercio_percent = (self.bottom_tercio_drag.css('bottom').split('px')[0] * 100) / circleH
	},

	setNextStep: function() {
		$('#helpers h3:not(.out):not(.done)').addClass('done').next().removeClass('out')
	},

	isStepDone: function(step) {
		return $('#helpers h3[data-step="' + step + '"]').hasClass('done')
	}
}
