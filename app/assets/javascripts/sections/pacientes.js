// Incluimos aquí el script para la sección de nuevo paciente

var pacientes_class = {
	new : function(view) {
		// Nuevo paciente
		view.find('.new-paciente').on('click', function(e) {
			$('#new_paciente').submit()
		})

		// Go back
		view.find('.go-back').on('click', function(e) {
			window.history.back()
		})
	}
}
