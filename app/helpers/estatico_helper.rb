module EstaticoHelper
  def porcentaje numerador, array
    unless numerador.nil? or array.nil?
      numerador / array.max
    else
      "No hay medidas suficientes para este calculo"
    end

  end

  def debora medicion, medida
    if medida.present? and medicion.present?
      if medicion.between?(medida.inferior,medida.superior)
        '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>'.html_safe
      else
        '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'.html_safe
      end
    end

  end

end
