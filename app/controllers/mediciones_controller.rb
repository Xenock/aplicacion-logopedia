class MedicionesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_medicione, only: [:show, :edit, :update, :destroy]

  # GET /mediciones
  # GET /mediciones.json
  def index
    @mediciones = Medicione.all
  end

  # GET /mediciones/1
  # GET /mediciones/1.json
  def show
  end

  # GET /mediciones/new
  def new
    @medicione = Medicione.new
    @paciente = Paciente.find(params[:paciente_id])
  end

  # GET /mediciones/1/edit
  def edit
  end

  # POST /mediciones
  # POST /mediciones.json
  def create
    @medicione = Medicione.new(medicione_params)

    respond_to do |format|
      if @medicione.save
        format.html { redirect_to estatico_resultados_path(medicione_id: @medicione.id), notice: 'Medicione was successfully created.' }
        format.json { render :show, status: :created, location: @medicione }
      else
        format.html { render :new }
        format.json { render json: @medicione.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mediciones/1
  # PATCH/PUT /mediciones/1.json
  def update
    respond_to do |format|
      if @medicione.update(medicione_params)
        format.html { redirect_to @medicione, notice: 'Medicione was successfully updated.' }
        format.json { render :show, status: :ok, location: @medicione }
      else
        format.html { render :edit }
        format.json { render json: @medicione.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mediciones/1
  # DELETE /mediciones/1.json
  def destroy
    @medicione.destroy
    respond_to do |format|
      format.html { redirect_to mediciones_url, notice: 'Medicione was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medicione
      @medicione = Medicione.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medicione_params
      params.require(:medicione).permit(
        :ojo_comisura_derecho,
        :ojo_comisura_izquierdo,
        :altura_labio_superior,
        :altura_labio_inferior,
        :leporino,
        :trichion_menton,
        :tercio_superior,
        :tercio_medio,
        :tercio_inferior,
        :tercio_inferior_superior,
        :tercio_inferior_medio,
        :tercio_inferior_inferior,
        :paciente_id)
    end
end
