class EstaticoController < ApplicationController
  before_action :authenticate_user!, only: [:perfil, :resultados, :grafica]
  def inicio
  end

  def perfil
    @paciente = Paciente.find(params[:paciente_id])
  end

  def resultados
    @medicione = Medicione.find(params[:medicione_id])
    @paciente = @medicione.paciente

    @ojo_comisura_derecho = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'ojo_comisura_derecho'
    ).first
    @ojo_comisura_izquierdo = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'ojo_comisura_izquierdo'
    ).first
    @altura_labio_superior = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'altura_labio_superior'
    ).first
    @altura_labio_inferior = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'altura_labio_inferior'
    ).first

    @cupido_derecho = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'cupido_derecho'
    ).first

    @cupido_izquierdo = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'cupido_izquierdo'
    ).first

    @leporino = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'leporino'
    ).first

    @trichion_menton = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'trichion_menton'
    ).first

    @distancia_intercisiva_maxima = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'distancia_intercisiva_maxima'
    ).first

    @tercio_superior = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'tercio_superior'
    ).first

    @tercio_medio = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'tercio_medio'
    ).first

    @tercio_inferior = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'tercio_inferior'
    ).first

    @tercio_inferior_superior = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'tercio_inferior_superior'
    ).first

    @tercio_inferior_medio = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'tercio_inferior_medio'
    ).first

    @tercio_inferior_inferior = Medida.where(
      sexo: @paciente.sexo,
      edad: Time.now.year-@paciente.fecha_nacimiento.year,
      zona: 'tercio_inferior_inferior'
    ).first

  end

  def grafica
    @medicion = Medicione.find(params[:medicione_id])
    @mediciones = Medicione.where(paciente_id: @medicion.paciente_id)
  end

  def tulogopeda
    @usuarios = User.all
  end
end
