json.array!(@mediciones) do |medicione|
  json.extract! medicione, :id, :ojo_derecho, :ojo_izquierdo, :oreja_izquierda, :oreja_derecha, :cupido_izquierdo, :cupido_derecho, :leporino, :paciente_id
  json.url medicione_url(medicione, format: :json)
end
