json.mediciones @paciente.medicione.each do |m|
  json.ojo_comisura_derecho m.ojo_comisura_derecho
  json.ojo_comisura_izquierdo m.ojo_comisura_izquierdo
  json.altura_labio_superior m.altura_labio_superior
  json.altura_labio_inferior m.altura_labio_inferior
  json.leporino m.leporino
  json.trichion_menton m.trichion_menton
  json.tercio_superior m.tercio_superior
  json.tercio_medio m.tercio_medio
  json.tercio_inferior m.tercio_inferior
  json.tercio_inferior_superior m.tercio_inferior_superior
  json.tercio_inferior_medio m.tercio_inferior_medio
  json.tercio_inferior_inferior m.tercio_inferior_inferior
  json.created_at m.created_at
end
