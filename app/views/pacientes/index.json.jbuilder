json.array!(@pacientes) do |paciente|
  json.extract! paciente, :id, :nombre, :fecha_nacimiento, :sexo, :motivo_consulta, :fecha_inicio_terapia
  json.url paciente_url(paciente, format: :json)
end
