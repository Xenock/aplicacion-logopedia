Rails.application.routes.draw do

  get 'imagenes/frontal'
  get 'imagenes/perfil'
  resources :pacientes, shallow: true do
    resources :mediciones
  end

  devise_for :users, controllers: {
               registrations: 'users/registrations'
             }
  get 'estatico/perfil'
  get 'estatico/resultados'
  get 'estatico/grafica'
  get 'estatico/tulogopeda'

  root to: "estatico#inicio"

end
